﻿using Prismactic.Core.Logging;

namespace Core.Tests.Modularity
{
    internal class MockLogger : ILoggerFacade
    {
        public string LastMessage;
        public Category LastMessageCategory;

        public void Log(string message, Category category, Priority priority, string memberName = "", string sourceFilePath = "",
            int sourceLineNumber = 0)
        {
            LastMessage = message;
            LastMessageCategory = category;
        }
    }
}