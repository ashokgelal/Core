﻿using Microsoft.Practices.ServiceLocation;
using System;
using Prismactic.Core.Extensions;
using Prismactic.Core.Logging;
using Prismactic.Core.Modularity;

namespace Core.Tests.Modularity
{
    internal class MockFactory
    {
        internal static ModuleInfo CreateModuleInfo(Type type, params string[] dependsOn)
        {
            var moduleInfo = new ModuleInfo(type.Name, type.AssemblyQualifiedName, "guid");
            moduleInfo.DependsOn.AddRange(dependsOn);
            return moduleInfo;
        }

        internal static ModuleInfo CreateModuleInfo(string name, params string[] dependsOn)
        {
            var moduleInfo = new ModuleInfo(name, name, "guid");
            moduleInfo.DependsOn.AddRange(dependsOn);
            return moduleInfo;
        }
    }

    internal class ExceptionThrowingModule : IModule
    {
        public void Initialize()
        {
            throw new InvalidOperationException("Intialization can't be performed");
        }
    }

    internal class NormalModule : IModule
    {
        internal static int InitializeCount;

        public void Initialize()
        {
            InitializeCount++;
        }
    }

    internal class InvalidModule
    {
    }

    internal class CustomModuleInitializerService : ModuleInitializer
    {
        public bool HandleModuleInitializerrorCalled;

        public CustomModuleInitializerService(IServiceLocator containerFacade, ILoggerFacade logger)
            : base(containerFacade, logger)
        {
        }

        public override void HandleModuleInitializationError(ModuleInfo moduleInfo, string assemblyName,
            Exception exception)
        {
            HandleModuleInitializerrorCalled = true;
        }
    }
}