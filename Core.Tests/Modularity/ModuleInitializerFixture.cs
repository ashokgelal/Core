﻿#region usings

using System;
using Microsoft.Practices.ServiceLocation;
using NSubstitute;
using NUnit.Framework;
using Prismactic.Core.Logging;
using Prismactic.Core.Modularity;

#endregion

namespace Core.Tests.Modularity
{
    [TestFixture]
    public class ModuleInitializerFixture
    {
        [Test]
        [ExpectedException(typeof (ModuleInitializeException))]
        public void InitializationExceptionsAreWrapped()
        {
            var moduleInfo = MockFactory.CreateModuleInfo(typeof (ExceptionThrowingModule));
            var loader = new ModuleInitializer(new MockSerivceLocator(), Substitute.For<ILoggerFacade>());
            loader.Initialize(moduleInfo);
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void NullContainerThrows()
        {
            var loader = new ModuleInitializer(null, Substitute.For<ILoggerFacade>());
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void NullLoggerThrows()
        {
            var loader = new ModuleInitializer(Substitute.For<IServiceLocator>(), null);
        }

        [Test]
        public void ShouldLogModuleInitializationError()
        {
            IServiceLocator containerFacade = new MockSerivceLocator();
            var logger = new MockLogger();
            var service = new ModuleInitializer(containerFacade, logger);
            var exceptionModule = MockFactory.CreateModuleInfo(typeof (ExceptionThrowingModule));

            try
            {
                service.Initialize(exceptionModule);
            }
            catch (ModuleInitializeException)
            {
            }

            Assert.IsNotNull(logger.LastMessage);
            StringAssert.Contains("ExceptionThrowingModule", logger.LastMessage);
        }

        [Test]
        public void ShouldLogModuleInitializeErrorsAndContinueLoading()
        {
            IServiceLocator containerFacade = new MockSerivceLocator();
            var service = new CustomModuleInitializerService(containerFacade, Substitute.For<ILoggerFacade>());
            var invalidModule = MockFactory.CreateModuleInfo(typeof (InvalidModule));

            Assert.IsFalse(service.HandleModuleInitializerrorCalled);
            service.Initialize(invalidModule);
            Assert.IsTrue(service.HandleModuleInitializerrorCalled);
        }

        [Test]
        public void ShouldResolveModuleAndInitializeSingleModule()
        {
            var service = new ModuleInitializer(new MockSerivceLocator(), Substitute.For<ILoggerFacade>());
            var info = MockFactory.CreateModuleInfo(typeof (NormalModule));
            service.Initialize(info);
            Assert.AreEqual(1, NormalModule.InitializeCount);
        }

        [Test]
        public void ShouldThrowExceptionIfBogusType()
        {
            var moduleInfo = new ModuleInfo("TestModule", "BadAssembly.BadType", "guid");

            var loader = new ModuleInitializer(new MockSerivceLocator(), new MockLogger());

            try
            {
                loader.Initialize(moduleInfo);
                Assert.Fail("Did not throw exception");
            }
            catch (ModuleInitializeException ex)
            {
                StringAssert.Contains("BadAssembly.BadType", ex.Message);
            }
            catch (Exception)
            {
                Assert.Fail();
            }
        }
    }
}