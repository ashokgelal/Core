﻿using NSubstitute;
using NUnit.Framework;
using System;
using Prismactic.Core.Logging;
using Prismactic.Core.Modularity;

namespace Core.Tests.Modularity
{
    [TestFixture]
    public class ModuleManagerFixture
    {
        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NullLoaderThrows()
        {
            new ModuleManager(null, Substitute.For<IModuleCatalog>(), Substitute.For<ILoggerFacade>());
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NullCatalogThrows()
        {
            new ModuleManager(Substitute.For<IModuleInitializer>(), null, Substitute.For<ILoggerFacade>());
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NullLoggerThrows()
        {

            new ModuleManager(Substitute.For<IModuleInitializer>(), Substitute.For<IModuleCatalog>(), null);
        }
    }
}