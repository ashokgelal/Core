﻿#region usings

using System;
using System.Collections.Generic;
using Microsoft.Practices.ServiceLocation;

#endregion

namespace Core.Tests.Modularity
{
    internal class MockSerivceLocator : ServiceLocatorImplBase
    {
        public Dictionary<Type, object> ResolvedInstances = new Dictionary<Type, object>();

        protected override object DoGetInstance(Type serviceType, string key)
        {
            object resolvedInstance;
            if (!ResolvedInstances.ContainsKey(serviceType))
            {
                resolvedInstance = Activator.CreateInstance(serviceType);
                ResolvedInstances.Add(serviceType, resolvedInstance);
            }
            else
            {
                resolvedInstance = ResolvedInstances[serviceType];
            }

            return resolvedInstance;
        }

        protected override IEnumerable<object> DoGetAllInstances(Type serviceType)
        {
            throw new NotImplementedException();
        }
    }
}