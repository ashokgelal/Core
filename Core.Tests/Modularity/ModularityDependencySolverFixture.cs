﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using Prismactic.Core.Modularity;

namespace Core.Tests.Modularity
{
    [TestFixture]
    public class ModuleDependencySolverFixture
    {
        private ModuleDependencySolver _solver;

        [SetUp]
        public void Init()
        {
            _solver = new ModuleDependencySolver();
        }


        [Test]
        public void CanAddModuleDepedency()
        {
            _solver.AddModule("ModuleA");
            _solver.AddModule("ModuleB");
            _solver.AddDependency("ModuleB", "ModuleA");
            Assert.AreEqual(2, _solver.ModuleCount);
        }

        [Test]
        public void CanAddModuleName()
        {
            _solver.AddModule("ModuleA");
            Assert.AreEqual(1, _solver.ModuleCount);
        }

        [Test]
        public void CanSolveAcyclicDependencies()
        {
            _solver.AddModule("ModuleA");
            _solver.AddModule("ModuleB");
            _solver.AddDependency("ModuleB", "ModuleA");
            string[] result = _solver.Solve();
            Assert.AreEqual(2, result.Length);
            Assert.AreEqual("ModuleA", result[0]);
            Assert.AreEqual("ModuleB", result[1]);
        }

        [Test]
        public void CanSolveForest()
        {
            _solver.AddModule("ModuleA");
            _solver.AddModule("ModuleB");
            _solver.AddModule("ModuleC");
            _solver.AddModule("ModuleD");
            _solver.AddModule("ModuleE");
            _solver.AddModule("ModuleF");
            _solver.AddDependency("ModuleC", "ModuleB");
            _solver.AddDependency("ModuleB", "ModuleA");
            _solver.AddDependency("ModuleE", "ModuleD");
            string[] result = _solver.Solve();
            Assert.AreEqual(6, result.Length);
            var test = new List<string>(result);
            Assert.IsTrue(test.IndexOf("ModuleA") < test.IndexOf("ModuleB"));
            Assert.IsTrue(test.IndexOf("ModuleB") < test.IndexOf("ModuleC"));
            Assert.IsTrue(test.IndexOf("ModuleD") < test.IndexOf("ModuleE"));
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void CannotAddDependencyWithoutAddingModule()
        {
            _solver.AddDependency("ModuleA", "ModuleB");
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void CannotAddEmptyModuleName()
        {
            _solver.AddModule(String.Empty);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void CannotAddNullModuleName()
        {
            _solver.AddModule(null);
        }

        [Test]
        [ExpectedException(typeof(CyclicDependencyFoundException))]
        public void FailsWithComplexCycle()
        {
            _solver.AddModule("ModuleA");
            _solver.AddModule("ModuleB");
            _solver.AddModule("ModuleC");
            _solver.AddModule("ModuleD");
            _solver.AddModule("ModuleE");
            _solver.AddModule("ModuleF");
            _solver.AddDependency("ModuleC", "ModuleB");
            _solver.AddDependency("ModuleB", "ModuleA");
            _solver.AddDependency("ModuleE", "ModuleD");
            _solver.AddDependency("ModuleE", "ModuleC");
            _solver.AddDependency("ModuleF", "ModuleE");
            _solver.AddDependency("ModuleD", "ModuleF");
            _solver.AddDependency("ModuleB", "ModuleD");
            _solver.Solve();
        }

        [Test]
        [ExpectedException(typeof(ModularityException))]
        public void FailsWithMissingModule()
        {
            _solver.AddModule("ModuleA");
            _solver.AddDependency("ModuleA", "ModuleB");
            _solver.Solve();
        }

        [Test]
        [ExpectedException(typeof(CyclicDependencyFoundException))]
        public void FailsWithSimpleCycle()
        {
            _solver.AddModule("ModuleB");
            _solver.AddDependency("ModuleB", "ModuleB");
            _solver.Solve();
        }

        [Test]
        public void ModuleDependencySolverIsAvailable()
        {
            Assert.IsNotNull(_solver);
        }
    }
}