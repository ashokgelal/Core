﻿#region usings

using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using Prismactic.Core.Modularity;

#endregion usings

namespace Core.Tests.Modularity
{
    [TestFixture]
    public class ModuleCatalogFixture
    {
        [Test]
        public void CanAddModule1()
        {
            var catalog = new ModuleCatalog();

            catalog.AddModule("Module", "ModuleType", "guid", null, "DependsOn1", "DependsOn2");

            Assert.AreEqual(1, catalog.Modules.Count());
            Assert.AreEqual("Module", catalog.Modules.First().ModuleName);
            Assert.AreEqual("ModuleType", catalog.Modules.First().ModuleType);
            Assert.AreEqual(2, catalog.Modules.First().DependsOn.Count);
            Assert.AreEqual("DependsOn1", catalog.Modules.First().DependsOn[0]);
            Assert.AreEqual("DependsOn2", catalog.Modules.First().DependsOn[1]);
        }

        [Test]
        public void CanAddModule2()
        {
            var catalog = new ModuleCatalog();

            var moduleInfo = Substitute.For<ModuleInfo>();

            catalog.AddModule(moduleInfo.GetType(), "guid", "DependsOn1", "DependsOn2");

            Assert.AreEqual(1, catalog.Modules.Count());
            Assert.AreEqual("ModuleInfoProxy", catalog.Modules.First().ModuleName);
            Assert.AreEqual(moduleInfo.GetType().AssemblyQualifiedName, catalog.Modules.First().ModuleType);
            Assert.AreEqual(2, catalog.Modules.First().DependsOn.Count);
            Assert.AreEqual("DependsOn1", catalog.Modules.First().DependsOn[0]);
            Assert.AreEqual("DependsOn2", catalog.Modules.First().DependsOn[1]);
        }

        [Test]
        public void CanAddModules()
        {
            var catalog = new ModuleCatalog();

            var fakeModule = Substitute.For<IModule>();
            catalog.AddModule(fakeModule.GetType(), "guid");

            Assert.AreEqual(1, catalog.Modules.Count());
            Assert.AreEqual("IModuleProxy", catalog.Modules.First().ModuleName);
        }

        [Test]
        public void CanCompleteListWithTheirDependencies()
        {
            // A <- B <- C
            var moduleInfoA = MockFactory.CreateModuleInfo("A");
            var moduleInfoB = MockFactory.CreateModuleInfo("B", "A");
            var moduleInfoC = MockFactory.CreateModuleInfo("C", "B");
            var moduleInfoOrphan = MockFactory.CreateModuleInfo("X", "B");

            var moduleInfos = new List<ModuleInfo> { moduleInfoA, moduleInfoB, moduleInfoC, moduleInfoOrphan };
            var moduleCatalog = new ModuleCatalog(moduleInfos);

            var dependantModules = moduleCatalog.CompleteListWithDependencies(new[] { moduleInfoC }).ToList();

            Assert.AreEqual(3, dependantModules.Count());
            Assert.IsTrue(dependantModules.Contains(moduleInfoA));
            Assert.IsTrue(dependantModules.Contains(moduleInfoB));
            Assert.IsTrue(dependantModules.Contains(moduleInfoC));
        }

        [Test]
        public void CanCreateCatalogFromList()
        {
            var moduleInfo = MockFactory.CreateModuleInfo("mock_module");
            var modules = new List<ModuleInfo> { moduleInfo };
            var moduleCatalog = new ModuleCatalog(modules);
            Assert.AreEqual(1, moduleCatalog.Modules.Count());
            Assert.AreEqual(moduleInfo, moduleCatalog.Modules.First());
        }

        [Test]
        public void CanGetDependenciesForModule()
        {
            // A <- B
            var moduleInfoA = MockFactory.CreateModuleInfo("A");
            var moduleInfoB = MockFactory.CreateModuleInfo("B", "A");
            var moduleInfos = new List<ModuleInfo> { moduleInfoA, moduleInfoB };
            var moduleCatalog = new ModuleCatalog(moduleInfos);

            var dependentModules = moduleCatalog.GetDependentModules(moduleInfoB).ToList();
            Assert.AreEqual(1, dependentModules.Count());
            Assert.AreEqual(moduleInfoA, dependentModules.First());
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CompleteListWithDependenciesThrowsWithNull()
        {
            var catalog = new ModuleCatalog();
            catalog.CompleteListWithDependencies(null);
        }

        [Test]
        public void ShouldReturnInCorrectRetrieveOrderWhenCompletingListWithDependencies()
        {
            // A <- B <- C <- D,    C <- X
            var moduleA = MockFactory.CreateModuleInfo("A");
            var moduleB = MockFactory.CreateModuleInfo("B", "A");
            var moduleC = MockFactory.CreateModuleInfo("C", "B");
            var moduleD = MockFactory.CreateModuleInfo("D", "C");
            var moduleX = MockFactory.CreateModuleInfo("X", "C");

            var moduleCatalog = new ModuleCatalog();
            // Add the modules in random order
            moduleCatalog.AddModule(moduleB);
            moduleCatalog.AddModule(moduleA);
            moduleCatalog.AddModule(moduleD);
            moduleCatalog.AddModule(moduleX);
            moduleCatalog.AddModule(moduleC);

            var dependantModules = moduleCatalog.CompleteListWithDependencies(new[] { moduleD, moduleX }).ToList();

            Assert.AreEqual(5, dependantModules.Count);
            Assert.IsTrue(dependantModules.IndexOf(moduleA) < dependantModules.IndexOf(moduleB));
            Assert.IsTrue(dependantModules.IndexOf(moduleB) < dependantModules.IndexOf(moduleC));
            Assert.IsTrue(dependantModules.IndexOf(moduleC) < dependantModules.IndexOf(moduleD));
            Assert.IsTrue(dependantModules.IndexOf(moduleC) < dependantModules.IndexOf(moduleX));
        }

        [Test]
        [ExpectedException(typeof(CyclicDependencyFoundException))]
        public void ShouldThrowOnCyclicDependency()
        {
            // A <- B <- C <- A
            var moduleInfoA = MockFactory.CreateModuleInfo("A", "C");
            var moduleInfoB = MockFactory.CreateModuleInfo("B", "A");
            var moduleInfoC = MockFactory.CreateModuleInfo("C", "B");

            var moduleInfos = new List<ModuleInfo> { moduleInfoA, moduleInfoB, moduleInfoC };
            new ModuleCatalog(moduleInfos).Validate();
        }

        [Test]
        [ExpectedException(typeof(DuplicateModuleException))]
        public void ShouldThrowOnDuplicateModule()
        {
            var moduleInfoA1 = MockFactory.CreateModuleInfo("A");
            var moduleInfoA2 = MockFactory.CreateModuleInfo("A");

            var moduleInfos = new List<ModuleInfo> { moduleInfoA1, moduleInfoA2 };
            new ModuleCatalog(moduleInfos).Validate();
        }

        [Test]
        [ExpectedException(typeof(ModularityException))]
        public void ShouldThrowOnMissingDependency()
        {
            var moduleInfoA = MockFactory.CreateModuleInfo("A", "B");
            var moduleInfos = new List<ModuleInfo> { moduleInfoA };
            new ModuleCatalog(moduleInfos).Validate();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ShowThrowTryingToCreateCatalogFromNullList()
        {
            var moduleCatalog = new ModuleCatalog(null);
            Assert.Fail("Should throw an exception");
        }
    }
}