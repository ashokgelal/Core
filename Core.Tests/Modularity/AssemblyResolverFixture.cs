﻿using NUnit.Framework;
using System;
using System.IO;
using Prismactic.Core.Modularity;

namespace Core.Tests.Modularity
{
    internal class AssemblyResolverFixture
    {
        private const string ModulesDirectory1 = @".\DynamicModules\MocksModulesAssemblyResolve";

//        [SetUp]
//        [TearDown]
//        public void CleanUpDirectories()
//        {
//            CompilerHelper.CleanUpDirectory(ModulesDirectory1);
//        }

        [Test]
        public void ShouldThrowOnInvalidAssemblyFilePath()
        {
            using (var resolver = new AssemblyResolver())
            {
                Assert.Throws<ArgumentNullException>(() => resolver.LoadAssemblyFrom(null));
                Assert.Throws<FileNotFoundException>(() => resolver.LoadAssemblyFrom("file://InexistentFile.dll"));
                Assert.Throws<ArgumentNullException>(() => resolver.LoadAssemblyFrom("InvalidUri.dll"));
            }
        }

//        [Test]
//        public void ShouldResolveTypeFromAbsoluteUriToAssembly()
//        {
//            string assemblyPath = CompilerHelper.GenerateDynamicModule("ModuleInLoadedFromContext1", "Module", ModulesDirectory1 + @"\ModuleInLoadedFromContext1.dll");
//            var uriBuilder = new UriBuilder
//            {
//                Host = String.Empty,
//                Scheme = Uri.UriSchemeFile,
//                Path = Path.GetFullPath(assemblyPath)
//            };
//            var assemblyUri = uriBuilder.Uri;
//            using (var resolver = new AssemblyResolver())
//            {
//                Type resolvedType =
//                    Type.GetType(
//                        "TestModules.ModuleInLoadedFromContext1Class, ModuleInLoadedFromContext1, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null");
//                Assert.IsNull(resolvedType);
//
//                resolver.LoadAssemblyFrom(assemblyUri.ToString());
//
//                resolvedType =
//                    Type.GetType(
//                        "TestModules.ModuleInLoadedFromContext1Class, ModuleInLoadedFromContext1, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null");
//                Assert.IsNotNull(resolvedType);
//            }
//        }

//        [Test]
//        public void ShouldResolvePartialAssemblyName()
//        {
//            string assemblyPath = CompilerHelper.GenerateDynamicModule("ModuleInLoadedFromContext2", "Module", ModulesDirectory1 + @"\ModuleInLoadedFromContext2.dll");
//            var uriBuilder = new UriBuilder
//            {
//                Host = String.Empty,
//                Scheme = Uri.UriSchemeFile,
//                Path = Path.GetFullPath(assemblyPath)
//            };
//            var assemblyUri = uriBuilder.Uri;
//            using (var resolver = new AssemblyResolver())
//            {
//                resolver.LoadAssemblyFrom(assemblyUri.ToString());
//
//                Type resolvedType =
//                    Type.GetType("TestModules.ModuleInLoadedFromContext2Class, ModuleInLoadedFromContext2");
//
//                Assert.IsNotNull(resolvedType);
//
//                resolvedType =
//                    Type.GetType("TestModules.ModuleInLoadedFromContext2Class, ModuleInLoadedFromContext2, Version=0.0.0.0");
//
//                Assert.IsNotNull(resolvedType);
//
//                resolvedType =
//                    Type.GetType("TestModules.ModuleInLoadedFromContext2Class, ModuleInLoadedFromContext2, Version=0.0.0.0, Culture=neutral");
//
//                Assert.IsNotNull(resolvedType);
//            }
//        }
    }

    internal class MockAssemblyResolver : IAssemblyResolver
    {
        public string LoadAssemblyFromArgument;
        public bool ThrowOnLoadAssemblyFrom;

        public void LoadAssemblyFrom(string assemblyFilePath)
        {
            LoadAssemblyFromArgument = assemblyFilePath;
            if (ThrowOnLoadAssemblyFrom)
                throw new Exception();
        }

        public void Dispose()
        {
        }
    }
}