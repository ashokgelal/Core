﻿#region usings

using System;
using Moq;
using NSubstitute;
using NUnit.Framework;
using Prismactic.Core.Extensions;
using Prismactic.Core.Modularity;

#endregion

namespace Core.Tests.Modularity
{
    [TestFixture]
    public class FileModuleTypeLoaderFixture
    {
        //        [Test]
//        public void CanRetrieveModule()
//        {
//            var assemblyResolver = new MockAssemblyResolver();
//            var retriever = new FileModuleTypeLoader(assemblyResolver);
//            var assembly = CompilerHelper.GenerateDynamicModule("FileModuleA", null);
//            var assemblyRef = "file://" + assembly;
//            var fileModuleInfo = CreateModuleInfo(assemblyRef, "TestModules.FileModuleAClass", "ModuleA", true, null);
//
//            var loadCompleted = false;
//            retriever.LoadModuleCompleted += delegate { loadCompleted = true; };
//
//            retriever.LoadModuleType(fileModuleInfo);
//
//            Assert.IsTrue(loadCompleted);
//            Assert.AreEqual(assemblyRef, assemblyResolver.LoadAssemblyFromArgument);
//        }

        private static ModuleInfo CreateModuleInfo(string assemblyFile, string moduleType, string moduleName,
            bool startupLoaded, params string[] dependsOn)
        {
            var moduleInfo = new ModuleInfo(moduleName, moduleType, "guid")
                             {
                                 Ref = assemblyFile,
                             };
            if (dependsOn != null)
            {
                moduleInfo.DependsOn.AddRange(dependsOn);
            }

            return moduleInfo;
        }

        [Test]
        public void CanRetrieveWithCorrectRef()
        {
            var retriever = new FileModuleTypeLoader();
            var moduleInfo = new ModuleInfo {Ref = "file://somefile"};

            Assert.IsTrue(retriever.CanLoadModuleType(moduleInfo));
        }

        [Test]
        public void CannotRetrieveWithIncorrectRef()
        {
            var retriever = new FileModuleTypeLoader();
            var moduleInfo = new ModuleInfo {Ref = "NotForLocalRetrieval"};

            Assert.IsFalse(retriever.CanLoadModuleType(moduleInfo));
        }

        [Test]
        public void FileModuleTypeLoaderCanBeDisposed()
        {
            var typeLoader = new FileModuleTypeLoader();
            var disposable = typeLoader as IDisposable;

            Assert.IsNotNull(disposable);
        }

        [Test]
        public void FileModuleTypeLoaderDisposeDoesNotThrowWithNonDisposableAssemblyResolver()
        {
            var typeLoader = new FileModuleTypeLoader(Substitute.For<IAssemblyResolver>());
            try
            {
                typeLoader.Dispose();
            }
            catch (Exception)
            {
                Assert.Fail();
            }
        }

        [Test]
        public void FileModuleTypeLoaderDisposeNukesAssemblyResolver()
        {
            var mockResolver = new Mock<IAssemblyResolver>();
            var disposableMockResolver = mockResolver.As<IDisposable>();
            disposableMockResolver.Setup(resolver => resolver.Dispose());

            var typeLoader = new FileModuleTypeLoader(mockResolver.Object);

            typeLoader.Dispose();

            disposableMockResolver.Verify(resolver => resolver.Dispose(), Times.Once());
        }

        [Test]
        public void ShouldReturnErrorToCallback()
        {
            var assemblyResolver = new MockAssemblyResolver();
            var retriever = new FileModuleTypeLoader(assemblyResolver);
            var fileModuleInfo = CreateModuleInfo("NonExistentFile.dll", "NonExistentModule", "NonExistent", true, null);

            assemblyResolver.ThrowOnLoadAssemblyFrom = true;
            Exception resultException = null;

            var loadCompleted = false;
            retriever.LoadModuleCompleted += delegate(object sender, LoadModuleCompletedEventArgs e)
                                             {
                                                 loadCompleted = true;
                                                 resultException = e.Error;
                                             };

            retriever.LoadModuleType(fileModuleInfo);

            Assert.IsTrue(loadCompleted);
            Assert.IsNotNull(resultException);
        }
    }
}