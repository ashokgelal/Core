﻿using NUnit.Framework;
using System;
using Prismactic.Core.Extensions;

namespace Core.Tests
{
    [TestFixture]
    public class ObjectExtensionsFixture
    {
        [Test]
        public void TestNullParameter_ThrowsException()
        {
            string param = null;
            // ReSharper disable once ExpressionIsAlwaysNull
            Assert.Throws<ArgumentNullException>(() => param.EnsureParameterNotNull("param"));
        }

        [Test]
        public void TestNonNullParameter_DoesnotThrowException()
        {
            var param = string.Empty;
            param.EnsureParameterNotNull("param");
            Assert.Pass();
        }
    }
}