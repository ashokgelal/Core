﻿#region usings

using System;
using System.Globalization;
using Microsoft.Practices.ServiceLocation;
using Prismactic.Core.Extensions;
using Prismactic.Core.Logging;
using Prismactic.Core.Properties;

#endregion

namespace Prismactic.Core.Modularity
{
    /// <summary>
    ///     Implements the <see cref="IModuleInitializer" /> interface. Handles loading of a module based on a type.
    /// </summary>
    public class ModuleInitializer : IModuleInitializer
    {
        private readonly ILoggerFacade _loggerFacade;
        private readonly IServiceLocator _serviceLocator;

        /// <summary>
        ///     Initializes a new instance of <see cref="ModuleInitializer" />.
        /// </summary>
        /// <param name="serviceLocator">The container that will be used to resolve the modules by specifying its type.</param>
        /// <param name="loggerFacade">The logger to use.</param>
        public ModuleInitializer(IServiceLocator serviceLocator, ILoggerFacade loggerFacade)
        {
            serviceLocator.EnsureParameterNotNull("servceLocator");
            loggerFacade.EnsureParameterNotNull("loggerFacade");
            _serviceLocator = serviceLocator;
            _loggerFacade = loggerFacade;
        }

        /// <summary>
        ///     Initializes the specified module.
        /// </summary>
        /// <param name="moduleInfo">The module to initialize</param>
        public void Initialize(ModuleInfo moduleInfo)
        {
            moduleInfo.EnsureParameterNotNull("moduleInfo");

            IModule moduleInstance = null;
            try
            {
                moduleInstance = CreateModule(moduleInfo);
                moduleInstance.Initialize();
            }
            catch (Exception ex)
            {
                HandleModuleInitializationError(moduleInfo,
                    moduleInstance != null ? moduleInstance.GetType().Assembly.FullName : null, ex);
            }
        }

        /// <summary>
        ///     Handles any exception ocurred in the module Initialization process,
        ///     logs the error using the <seealso cref="ILoggerFacade" /> and throws a <seealso cref="ModuleInitializeException" />
        ///     .
        ///     This method can be overriden to provide a different behavior.
        /// </summary>
        /// <param name="moduleInfo">The module metadata where the error happenened.</param>
        /// <param name="assemblyName">The assembly name.</param>
        /// <param name="exception">The exception thrown that is the cause of the current error.</param>
        /// <exception cref="ModuleInitializeException"></exception>
        public virtual void HandleModuleInitializationError(ModuleInfo moduleInfo, string assemblyName,
            Exception exception)
        {
            moduleInfo.EnsureParameterNotNull("moduleInfo");
            exception.EnsureParameterNotNull("exception");

            Exception moduleException;

            if (exception is ModuleInitializeException)
            {
                moduleException = exception;
            }
            else
            {
                if (!string.IsNullOrEmpty(assemblyName))
                {
                    moduleException = new ModuleInitializeException(moduleInfo.ModuleName, assemblyName,
                        exception.Message, exception);
                }
                else
                {
                    moduleException = new ModuleInitializeException(moduleInfo.ModuleName, exception.Message, exception);
                }
            }

            _loggerFacade.Log(moduleException.ToString(), Category.Exception, Priority.High);

            throw moduleException;
        }

        /// <summary>
        ///     Uses the container to resolve a new <see cref="IModule" /> by specifying its <see cref="Type" />.
        /// </summary>
        /// <param name="moduleInfo">The module to create.</param>
        /// <returns>A new instance of the module specified by <paramref name="moduleInfo" />.</returns>
        protected virtual IModule CreateModule(ModuleInfo moduleInfo)
        {
            moduleInfo.EnsureParameterNotNull("moduleInfo");
            var msg = string.Format("Creating module for module info: {0}", moduleInfo);
            _loggerFacade.Log(msg, Category.Debug, Priority.Low);
            return CreateModule(moduleInfo.ModuleType);
        }

        /// <summary>
        ///     Uses the container to resolve a new <see cref="IModule" /> by specifying its <see cref="Type" />.
        /// </summary>
        /// <param name="typeName">The type name to resolve. This type must implement <see cref="IModule" />.</param>
        /// <returns>A new instance of <paramref name="typeName" />.</returns>
        protected virtual IModule CreateModule(string typeName)
        {
            var msg = string.Format("Creating module for type: {0}", typeName);
            _loggerFacade.Log(msg, Category.Debug, Priority.Low);
            var moduleType = Type.GetType(typeName);
            if (moduleType == null)
            {
                throw new ModuleInitializeException(string.Format(CultureInfo.CurrentCulture, Resources.FailedToGetType,
                    typeName));
            }

            return (IModule) _serviceLocator.GetInstance(moduleType);
        }
    }
}