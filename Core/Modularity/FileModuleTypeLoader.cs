﻿#region usings

using System;
using System.Collections.Generic;
using Prismactic.Core.Extensions;

#endregion usings

namespace Prismactic.Core.Modularity
{
    /// <summary>
    ///     Loads modules from an arbitrary location on the filesystem. This typeloader is only called if
    ///     <see cref="ModuleInfo" /> classes have a Ref parameter that starts with "file://".
    ///     This class is only used on the Desktop version of the Composite Application Library.
    /// </summary>
    public class FileModuleTypeLoader : IModuleTypeLoader, IDisposable
    {
        private const string REF_FILE_PREFIX = "file://";

        private readonly IAssemblyResolver _assemblyResolver;
        private readonly HashSet<Uri> _downloadedUris;

        /// <summary>
        ///     Initializes a new instance of the <see cref="FileModuleTypeLoader" /> class.
        /// </summary>
        public FileModuleTypeLoader()
            : this(new AssemblyResolver())
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="FileModuleTypeLoader" /> class.
        /// </summary>
        /// <param name="assemblyResolver">The assembly resolver.</param>
        public FileModuleTypeLoader(IAssemblyResolver assemblyResolver)
        {
            _downloadedUris = new HashSet<Uri>();
            _assemblyResolver = assemblyResolver;
        }

        /// <summary>
        ///     Raised when a module is loaded or fails to load.
        /// </summary>
        public event EventHandler<LoadModuleCompletedEventArgs> LoadModuleCompleted;

        /// <summary>
        ///     Evaluates the <see cref="ModuleInfo.Ref" /> property to see if the current typeloader will be able to retrieve the
        ///     <paramref name="moduleInfo" />.
        ///     Returns true if the <see cref="ModuleInfo.Ref" /> property starts with "file://", because this indicates that the
        ///     file
        ///     is a local file.
        /// </summary>
        /// <param name="moduleInfo">Module that should have it's type loaded.</param>
        /// <returns>
        ///     <see langword="true" /> if the current typeloader is able to retrieve the module, otherwise
        ///     <see langword="false" />.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     An <see cref="ArgumentNullException" /> is thrown if
        ///     <paramref name="moduleInfo" /> is null.
        /// </exception>
        public bool CanLoadModuleType(ModuleInfo moduleInfo)
        {
            moduleInfo.EnsureParameterNotNull("moduleInfo");
            return moduleInfo.Ref != null && moduleInfo.Ref.StartsWith(REF_FILE_PREFIX, StringComparison.Ordinal);
        }

        /// <summary>
        ///     Retrieves the <paramref name="moduleInfo" />.
        /// </summary>
        /// <param name="moduleInfo">Module that should have it's type loaded.</param>
        public virtual void LoadModuleType(ModuleInfo moduleInfo)
        {
            moduleInfo.EnsureParameterNotNull("moduleInfo");
            try
            {
                var uri = new Uri(moduleInfo.Ref, UriKind.RelativeOrAbsolute);

                // If this module has already been downloaded, fire the completed event.
                if (IsSuccessfullyDownloaded(uri))
                {
                    RaiseLoadModuleCompleted(moduleInfo, null);
                }
                else
                {
                    _assemblyResolver.LoadAssemblyFrom(moduleInfo.Ref);
                    RecordDownloadSuccess(uri);
                    RaiseLoadModuleCompleted(moduleInfo, null);
                }
            }
            catch (Exception ex)
            {
                RaiseLoadModuleCompleted(moduleInfo, ex);
            }
        }

        protected void RaiseLoadModuleCompleted(ModuleInfo moduleInfo, Exception error)
        {
            RaiseLoadModuleCompleted(new LoadModuleCompletedEventArgs(moduleInfo, error));
        }

        protected void RaiseLoadModuleCompleted(LoadModuleCompletedEventArgs e)
        {
            if (LoadModuleCompleted != null)
            {
                LoadModuleCompleted(this, e);
            }
        }

        protected bool IsSuccessfullyDownloaded(Uri uri)
        {
            lock (_downloadedUris)
            {
                return _downloadedUris.Contains(uri);
            }
        }

        protected void RecordDownloadSuccess(Uri uri)
        {
            lock (_downloadedUris)
            {
                _downloadedUris.Add(uri);
            }
        }

        #region Implementation of IDisposable

        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <remarks>Calls <see cref="Dispose(bool)" /></remarks>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///     Disposes the associated <see cref="AssemblyResolver" />.
        /// </summary>
        /// <param name="disposing">When <see langword="true" />, it is being called from the Dispose method.</param>
        protected virtual void Dispose(bool disposing)
        {
            var disposableResolver = _assemblyResolver as IDisposable;
            if (disposableResolver != null)
            {
                disposableResolver.Dispose();
            }
        }

        #endregion Implementation of IDisposable
    }
}