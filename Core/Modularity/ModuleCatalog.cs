﻿#region usings

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using Prismactic.Core.Extensions;
using Prismactic.Core.Properties;

#endregion usings

namespace Prismactic.Core.Modularity
{
    /// <summary>
    ///     The <see cref="ModuleCatalog" /> holds information about the modules that can be used by the
    ///     application. Each module is described in a <see cref="ModuleInfo" /> class, that records the
    ///     name, type and location of the module.
    ///     It also verifies that the <see cref="ModuleCatalog" /> is internally valid. That means that
    ///     it does not have:
    ///     <list>
    ///         <item>Circular dependencies</item>
    ///         <item>Missing dependencies</item>
    ///     </list>
    ///     The <see cref="ModuleCatalog" /> also serves as a baseclass for more specialized Catalogs .
    /// </summary>
    public class ModuleCatalog : IModuleCatalog
    {
        private readonly ModuleInfoItemCollection _moduleInfoItemCollection;
        private bool _isLoaded;

        public ModuleCatalog()
        {
            _moduleInfoItemCollection = new ModuleInfoItemCollection();
            _moduleInfoItemCollection.CollectionChanged += ModuleInfoItemCollectionChangedHandler;
        }

        public ModuleCatalog(IEnumerable<ModuleInfo> modules)
            : this()
        {
            modules.EnsureParameterNotNull("modules");
            foreach (var moduleInfo in modules)
            {
                _moduleInfoItemCollection.Add(moduleInfo);
            }
        }

        /// <summary>
        ///     Gets or sets a value that remembers whether the <see cref="ModuleCatalog" /> has been validated already.
        /// </summary>
        protected bool Validated { get; set; }

        public IEnumerable<ModuleInfo> Modules
        {
            get { return _moduleInfoItemCollection; }
        }

        /// <summary>
        ///     Adds a <see cref="ModuleInfo" /> to the <see cref="ModuleCatalog" />.
        /// </summary>
        /// <param name="moduleInfo">The <see cref="ModuleInfo" /> to add.</param>
        /// <returns>The <see cref="ModuleCatalog" /> for easily adding multiple modules.</returns>
        public virtual void AddModule(ModuleInfo moduleInfo)
        {
            moduleInfo.EnsureParameterNotNull("moduleInfo");
            _moduleInfoItemCollection.Add(moduleInfo);
        }

        /// <summary>
        ///     Returns a list of <see cref="ModuleInfo" />s that contain both the <see cref="ModuleInfo" />s in
        ///     <paramref name="modules" />, but also all the modules they depend on.
        /// </summary>
        /// <param name="modules">The modules to get the dependencies for.</param>
        /// <returns>
        ///     A list of <see cref="ModuleInfo" /> that contains both all <see cref="ModuleInfo" />s in
        ///     <paramref name="modules" />
        ///     but also all the <see cref="ModuleInfo" /> they depend on.
        /// </returns>
        public virtual IEnumerable<ModuleInfo> CompleteListWithDependencies(IEnumerable<ModuleInfo> modules)
        {
            modules.EnsureParameterNotNull("modules");
            EnsureCatalogValidated();

            var completeList = new List<ModuleInfo>();
            var pendingList = modules.ToList();
            while (pendingList.Count > 0)
            {
                var moduleInfo = pendingList[0];

                foreach (var dependency in GetDependentModules(moduleInfo))
                {
                    if (!completeList.Contains(dependency) && !pendingList.Contains(dependency))
                    {
                        pendingList.Add(dependency);
                    }
                }

                pendingList.RemoveAt(0);
                completeList.Add(moduleInfo);
            }

            return Sort(completeList);
        }

        /// <summary>
        ///     Return the list of <see cref="ModuleInfo" />s that <paramref name="moduleInfo" /> depends on.
        /// </summary>
        /// <remarks>
        ///     If  the <see cref="ModuleCatalog" /> was not yet validated, this method will call <see cref="Validate" />.
        /// </remarks>
        /// <param name="moduleInfo">The <see cref="ModuleInfo" /> to get the </param>
        /// <returns>An enumeration of <see cref="ModuleInfo" /> that <paramref name="moduleInfo" /> depends on.</returns>
        public IEnumerable<ModuleInfo> GetDependentModules(ModuleInfo moduleInfo)
        {
            EnsureCatalogValidated();
            return GetDependentModulesWithValidation(moduleInfo);
        }

        /// <summary>
        ///     Initializes the catalog, which may load and validate the modules.
        /// </summary>
        /// <exception cref="ModularityException">
        ///     When validation of the <see cref="ModuleCatalog" /> fails, because this method
        ///     calls <see cref="Validate" />.
        /// </exception>
        public virtual void Initialize()
        {
            if (!_isLoaded)
            {
                Load();
            }

            Validate();
        }

        /// <summary>
        ///     Loads the catalog if necessary.
        /// </summary>
        public void Load()
        {
            _isLoaded = true;
            InnerLoad();
        }

        /// <summary>
        ///     Does the actual work of loading the catalog.  The base implementation does nothing.
        /// </summary>
        protected virtual void InnerLoad()
        {
        }

        /// <summary>
        ///     Adds a groupless <see cref="ModuleInfo" /> to the catalog.
        /// </summary>
        /// <param name="moduleName">Name of the module to be added.</param>
        /// <param name="moduleType"><see cref="Type" /> of the module to be added.</param>
        /// <param name="guid">A unique identifier for the module.</param>
        /// <param name="refValue">Reference to the location of the module to be added assembly.</param>
        /// <param name="dependsOn">
        ///     Collection of module names (<see cref="ModuleInfo.ModuleName" />) of the modules on which the
        ///     module to be added logically depends on.
        /// </param>
        /// <returns>The same <see cref="ModuleCatalog" /> instance with the added module.</returns>
        public ModuleCatalog AddModule(string moduleName, string moduleType, string guid, string refValue,
            params string[] dependsOn)
        {
            moduleName.EnsureParameterNotNull("moduleName");
            moduleType.EnsureParameterNotNull("moduleType");
            var moduleInfo = new ModuleInfo(moduleName, moduleType, guid);
            moduleInfo.DependsOn.AddRange(dependsOn);
            moduleInfo.Ref = refValue;
            _moduleInfoItemCollection.Add(moduleInfo);
            return this;
        }

        /// <summary>
        ///     Adds a groupless <see cref="ModuleInfo" /> to the catalog.
        /// </summary>
        /// <param name="moduleType"><see cref="Type" /> of the module to be added.</param>
        /// <param name="guid">A unique identifier for the module.</param>
        /// <param name="dependsOn">
        ///     Collection of module names (<see cref="ModuleInfo.ModuleName" />) of the modules on which the
        ///     module to be added logically depends on.
        /// </param>
        /// <returns>The same <see cref="ModuleCatalog" /> instance with the added module.</returns>
        public ModuleCatalog AddModule(Type moduleType, string guid, params string[] dependsOn)
        {
            moduleType.EnsureParameterNotNull("moduleType");
            guid.EnsureParameterNotNull("guid");
            return AddModule(moduleType.Name, moduleType.AssemblyQualifiedName, guid, dependsOn);
        }

        /// <summary>
        ///     Adds a groupless <see cref="ModuleInfo" /> to the catalog.
        /// </summary>
        /// <param name="moduleName">Name of the module to be added.</param>
        /// <param name="moduleType"><see cref="Type" /> of the module to be added.</param>
        /// <param name="guid">A unique identifier for the module.</param>
        /// <param name="dependsOn">
        ///     Collection of module names (<see cref="ModuleInfo.ModuleName" />) of the modules on which the
        ///     module to be added logically depends on.
        /// </param>
        /// <returns>The same <see cref="ModuleCatalog" /> instance with the added module.</returns>
        public ModuleCatalog AddModule(string moduleName, string moduleType, string guid, params string[] dependsOn)
        {
            return AddModule(moduleName, moduleType, guid, null, dependsOn);
        }

        /// <summary>
        ///     Validates the <see cref="ModuleCatalog" />.
        /// </summary>
        /// <exception cref="ModularityException">When validation of the <see cref="ModuleCatalog" /> fails.</exception>
        public virtual void Validate()
        {
            ValidateUniqueModules();
            ValidateDependencyGraph();
            Validated = true;
        }

        /// <summary>
        ///     Checks for cyclic dependencies, by calling the dependencysolver.
        /// </summary>
        /// <param name="modules">the.</param>
        /// <returns></returns>
        protected static string[] SolveDependencies(IEnumerable<ModuleInfo> modules)
        {
            modules.EnsureParameterNotNull("modules");

            var solver = new ModuleDependencySolver();

            foreach (var data in modules)
            {
                solver.AddModule(data.ModuleName);

                if (data.DependsOn == null) continue;
                foreach (var dependsOnModule in data.DependsOn)
                {
                    solver.AddDependency(data.ModuleName, dependsOnModule);
                }
            }

            return solver.ModuleCount > 0 ? solver.Solve() : new string[0];
        }

        /// <summary>
        ///     Ensures that the catalog is validated.
        /// </summary>
        protected virtual void EnsureCatalogValidated()
        {
            if (!Validated)
            {
                Validate();
            }
        }

        /// <summary>
        ///     Returns the <see cref="ModuleInfo" /> on which the received module dependens on.
        /// </summary>
        /// <param name="moduleInfo">Module whose dependant modules are requested.</param>
        /// <returns>Collection of <see cref="ModuleInfo" /> dependants of <paramref name="moduleInfo" />.</returns>
        protected virtual IEnumerable<ModuleInfo> GetDependentModulesWithValidation(ModuleInfo moduleInfo)
        {
            return Modules.Where(dependantModule => moduleInfo.DependsOn.Contains(dependantModule.ModuleName));
        }

        /// <summary>
        ///     Sorts a list of <see cref="ModuleInfo" />s. This method is called by <see cref="CompleteListWithDependencies" />
        ///     to return a sorted list.
        /// </summary>
        /// <param name="modules">The <see cref="ModuleInfo" />s to sort.</param>
        /// <returns>Sorted list of <see cref="ModuleInfo" />s</returns>
        protected virtual IEnumerable<ModuleInfo> Sort(IEnumerable<ModuleInfo> modules)
        {
            modules.EnsureParameterNotNull("modules");
            var modulesList = modules.ToList();
            return
                SolveDependencies(modulesList).Select(moduleName => modulesList.First(m => m.ModuleName == moduleName));
        }

        /// <summary>
        ///     Ensures that there are no cyclic dependencies.
        /// </summary>
        protected virtual void ValidateDependencyGraph()
        {
            SolveDependencies(Modules);
        }

        /// <summary>
        ///     Makes sure all modules have an Unique name.
        /// </summary>
        /// <exception cref="DuplicateModuleException">
        ///     Thrown if the names of one or more modules are not unique.
        /// </exception>
        protected virtual void ValidateUniqueModules()
        {
            var moduleNames = Modules.Select(m => m.ModuleName).ToList();

            var duplicateModule = moduleNames.FirstOrDefault(m => moduleNames.Count(m2 => m2 == m) > 1);

            if (duplicateModule != null)
            {
                throw new DuplicateModuleException(duplicateModule,
                    String.Format(CultureInfo.CurrentCulture, Resources.DuplicatedModule, duplicateModule));
            }
        }

        private void ModuleInfoItemCollectionChangedHandler(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (Validated)
            {
                EnsureCatalogValidated();
            }
        }

        private class ModuleInfoItemCollection : Collection<ModuleInfo>, INotifyCollectionChanged
        {
            public event NotifyCollectionChangedEventHandler CollectionChanged;

            protected override void InsertItem(int index, ModuleInfo item)
            {
                base.InsertItem(index, item);
                OnNotifyCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item,
                    index));
            }

            protected void OnNotifyCollectionChanged(NotifyCollectionChangedEventArgs eventArgs)
            {
                if (CollectionChanged != null)
                {
                    CollectionChanged(this, eventArgs);
                }
            }
        }
    }
}