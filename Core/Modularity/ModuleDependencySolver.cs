﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Prismactic.Core.Extensions;
using Prismactic.Core.Properties;

namespace Prismactic.Core.Modularity
{
    public class ModuleDependencySolver
    {
        private readonly ListDictionary<string, string> _dependencyMatrix;
        private readonly List<string> _knownModules;

        /// <summary>
        ///     Gets the number of modules added to the solver.
        /// </summary>
        /// <value>The number of modules.</value>
        public int ModuleCount
        {
            get { return _dependencyMatrix.Count; }
        }

        public ModuleDependencySolver()
        {
            _dependencyMatrix = new ListDictionary<string, string>();
            _knownModules = new List<string>();
        }

        /// <summary>
        ///     Adds a module dependency between the modules specified by dependingModule and
        ///     dependentModule.
        /// </summary>
        /// <param name="dependerModule">The name of the module with the dependency.</param>
        /// <param name="dependsOnModule">The name of the module dependerModule depends on.</param>
        public void AddDependency(string dependerModule, string dependsOnModule)
        {
            dependerModule.EnsureStringNotNullOrWhitespace("dependerModule", Resources.ModuleNameCannotBeNullOrEmpty);
            dependsOnModule.EnsureStringNotNullOrWhitespace("dependsOnModule", Resources.ModuleNameCannotBeNullOrEmpty);
            if (!_knownModules.Contains(dependerModule))
            {
                throw new ArgumentException(string.Format(CultureInfo.CurrentCulture,
                    Resources.DependencyForUnknownModule, dependerModule));
            }
            AddToDependencyMatrix(dependsOnModule);
            _dependencyMatrix.Add(dependsOnModule, dependerModule);
        }

        /// <summary>
        ///     Adds a module to the solver.
        /// </summary>
        /// <param name="moduleName">The name that uniquely identifies the module.</param>
        public void AddModule(string moduleName)
        {
            moduleName.EnsureStringNotNullOrWhitespace(Resources.ModuleNameCannotBeNullOrEmpty, "moduleName");
            AddToDependencyMatrix(moduleName);
            AddToKnownModules(moduleName);
        }

        /// <summary>
        ///     Calculates an ordered vector according to the defined dependencies.
        ///     Non-dependant modules appears at the beginning of the resulting array.
        /// </summary>
        /// <returns>The resulting ordered list of modules.</returns>
        /// <exception cref="CyclicDependencyFoundException">
        ///     This exception is thrown when a cycle is found in the defined depedency graph.
        /// </exception>
        /// <exception cref="ModularityException">
        ///     This exception is thrown when a module declared a dependency on another module which is not declared to be loaded.
        /// </exception>
        public string[] Solve()
        {
            var skip = new List<string>();
            while (skip.Count < _dependencyMatrix.Count)
            {
                var leaves = FindLeaves(skip);
                if (leaves.Count == 0 && skip.Count < _dependencyMatrix.Count)
                {
                    throw new CyclicDependencyFoundException(Resources.CyclicDependencyFound);
                }
                skip.AddRange(leaves);
            }
            skip.Reverse();

            if (skip.Count <= _knownModules.Count) return skip.ToArray();
            var moduleNames = FindMissingModules(skip);
            throw new ModularityException(moduleNames,
                String.Format(CultureInfo.CurrentCulture, Resources.DependencyOnMissingModule, moduleNames));
        }

        private void AddToDependencyMatrix(string moduleName)
        {
            if (!_dependencyMatrix.ContainsKey(moduleName))
            {
                _dependencyMatrix.Add(moduleName);
            }
        }

        private void AddToKnownModules(string moduleName)
        {
            if (!_knownModules.Contains(moduleName))
            {
                _knownModules.Add(moduleName);
            }
        }

        private List<string> FindLeaves(ICollection<string> skip)
        {
            var result = from precedent in _dependencyMatrix.Keys
                                         where !skip.Contains(precedent)
                                         let count = _dependencyMatrix[precedent].Count(dependent => !skip.Contains(dependent))
                                         where count == 0
                                         select precedent;
            return result.ToList();
        }

        private string FindMissingModules(IEnumerable<string> modules)
        {
            var missingModules = modules.Where(module => !_knownModules.Contains(module)).ToList();
            return string.Join(", ", missingModules);
        }
    }
}