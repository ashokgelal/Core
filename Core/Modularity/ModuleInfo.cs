﻿#region usings

using System;
using System.Collections.ObjectModel;
using Prismactic.Core.Extensions;

#endregion

namespace Prismactic.Core.Modularity
{
    /// <summary>
    ///     Defines the metadata that describes a module.
    /// </summary>
    public class ModuleInfo
    {
        /// <summary>
        ///     Initializes a new empty instance of <see cref="ModuleInfo" />.
        /// </summary>
        public ModuleInfo()
            : this(null, null, null, new string[0])
        {
        }

        /// <summary>
        ///     Initializes a new instance of <see cref="ModuleInfo" />.
        /// </summary>
        /// <param name="name">The module's name.</param>
        /// <param name="type">The module's type.</param>
        /// <param name="guid">The module's unique id</param>
        public ModuleInfo(string name, string type, string guid)
            : this(name, type, guid, new string[0])
        {
        }

        /// <summary>
        ///     Initializes a new instance of <see cref="ModuleInfo" />.
        /// </summary>
        /// <param name="name">The module's name.</param>
        /// <param name="type">The module <see cref="Type" />'s AssemblyQualifiedName.</param>
        /// <param name="dependsOn">The modules this instance depends on.</param>
        /// <param name="guid">The module's unique id</param>
        /// <exception cref="ArgumentNullException">
        ///     An <see cref="ArgumentNullException" /> is thrown if
        ///     <paramref name="dependsOn" /> is <see langword="null" />.
        /// </exception>
        public ModuleInfo(string name, string type, string guid, params string[] dependsOn)
        {
            dependsOn.EnsureParameterNotNull("dependsOn");

            ModuleName = name;
            ModuleType = type;
            Guid = guid;
            DependsOn = new Collection<string>();
            foreach (var dependency in dependsOn)
            {
                DependsOn.Add(dependency);
            }
        }

        /// <summary>
        ///     Gets or sets a unique id of the module.
        /// </summary>
        public string Guid { get; set; }

        /// <summary>
        ///     Gets or sets the list of modules that this module depends upon.
        /// </summary>
        /// <value>The list of modules that this module depends upon.</value>
        public Collection<string> DependsOn { get; set; }

        /// <summary>
        ///     Gets or sets the name of the module.
        /// </summary>
        /// <value>The name of the module.</value>
        public string ModuleName { get; set; }

        /// <summary>
        ///     Gets or sets the module <see cref="Type" />'s AssemblyQualifiedName.
        /// </summary>
        /// <value>The type of the module.</value>
        public string ModuleType { get; set; }

        /// <summary>
        ///     Reference to the location of the module assembly.
        ///     <example> file://c:/MyProject/Modules/MyModule.dll for a loose DLL. </example>
        /// </summary>
        public string Ref { get; set; }

        /// <summary>
        ///     Gets or sets the state of the <see cref="ModuleInfo" /> with regards to the module loading and initialization
        ///     process.
        /// </summary>
        public ModuleState State { get; set; }
    }
}