﻿#region usings

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Prismactic.Core.Extensions;
using Prismactic.Core.Logging;
using Prismactic.Core.Properties;

#endregion usings

namespace Prismactic.Core.Modularity
{
    /// <summary>
    ///     Component responsible for coordinating the modules' type loading and module initialization process.
    /// </summary>
    public class ModuleManager : IModuleManager, IDisposable
    {
        private readonly ILoggerFacade _loggerFacade;
        private readonly IModuleInitializer _moduleInitializer;
        private readonly HashSet<IModuleTypeLoader> _subscribedToModuleTypeLoaders;
        private IEnumerable<IModuleTypeLoader> _typeLoaders;
        public event EventHandler<LoadModuleCompletedEventArgs> LoadModuleCompleted;

        /// <summary>
        ///     Initializes an instance of the <see cref="ModuleManager" /> class.
        /// </summary>
        /// <param name="moduleInitializer">Service used for initialization of modules.</param>
        /// <param name="moduleCatalog">Catalog that enumerates the modules to be loaded and initialized.</param>
        /// <param name="loggerFacade">Logger used during the load and initialization of modules.</param>
        public ModuleManager(IModuleInitializer moduleInitializer, IModuleCatalog moduleCatalog,
            ILoggerFacade loggerFacade)
        {
            moduleInitializer.EnsureParameterNotNull("moduleInitializer");
            moduleCatalog.EnsureParameterNotNull("moduleCatalog");
            loggerFacade.EnsureParameterNotNull("loggerFacade");

            _moduleInitializer = moduleInitializer;
            ModuleCatalog = moduleCatalog;
            _loggerFacade = loggerFacade;
            _subscribedToModuleTypeLoaders = new HashSet<IModuleTypeLoader>();
        }

        /// <summary>
        ///     The module catalog specified in the constructor.
        /// </summary>
        protected IModuleCatalog ModuleCatalog { get; private set; }

        /// <summary>
        ///     Returns the list of registered <see cref="IModuleTypeLoader" /> instances that will be
        ///     used to load the types of modules.
        /// </summary>
        /// <value>The module type loaders.</value>
        public virtual IEnumerable<IModuleTypeLoader> ModuleTypeLoaders
        {
            get
            {
                return _typeLoaders ?? (_typeLoaders = new List<IModuleTypeLoader> { new FileModuleTypeLoader() });
            }

            set { _typeLoaders = value; }
        }

        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <remarks>Calls <see cref="Dispose(bool)" /></remarks>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        /// <summary>
        ///     Initializes all the modules from the <see cref="ModuleCatalog" />.
        /// </summary>
        public void Run()
        {
            ModuleCatalog.Initialize();
            var modulesToLoadTypes = ModuleCatalog.CompleteListWithDependencies(ModuleCatalog.Modules);
            if (modulesToLoadTypes != null)
            {
                LoadModuleTypes(modulesToLoadTypes);
            }
        }

        /// <summary>
        ///     Loads and initializes the module on the <see cref="ModuleCatalog" /> with the name <paramref name="moduleName" />.
        /// </summary>
        /// <param name="moduleName">Name of the module requested for initialization.</param>
        public void LoadModule(string moduleName)
        {
            var module = ModuleCatalog.Modules.Where(m => m.ModuleName == moduleName);
            var modules = module as IList<ModuleInfo> ?? module.ToList();
            if (module == null || modules.Count() != 1)
            {
                throw new ModuleNotFoundException(moduleName,
                    string.Format(CultureInfo.CurrentCulture, Resources.ModuleNotFound, moduleName));
            }

            var modulesToLoad = ModuleCatalog.CompleteListWithDependencies(modules);

            LoadModuleTypes(modulesToLoad);
        }

        private void LoadModuleTypes(IEnumerable<ModuleInfo> moduleInfos)
        {
            if (moduleInfos == null)
            {
                return;
            }

            foreach (var moduleInfo in moduleInfos.Where(moduleInfo => moduleInfo.State == ModuleState.NotStarted))
            {
                if (ModuleNeedsRetrieval(moduleInfo))
                {
                    BeginRetrievingModule(moduleInfo);
                }
                else
                {
                    moduleInfo.State = ModuleState.ReadyForInitialization;
                }
            }

            LoadModulesThatAreReadyForLoad();
        }

        /// <summary>
        ///     Loads the modules that are not intialized and have their dependencies loaded.
        /// </summary>
        protected void LoadModulesThatAreReadyForLoad()
        {
            var keepLoading = true;
            while (keepLoading)
            {
                keepLoading = false;
                var availableModules =
                    ModuleCatalog.Modules.Where(m => m.State == ModuleState.ReadyForInitialization);

                foreach (var moduleInfo in availableModules)
                {
                    if ((moduleInfo.State != ModuleState.Initialized) && (AreDependenciesLoaded(moduleInfo)))
                    {
                        moduleInfo.State = ModuleState.Initializing;
                        InitializeModule(moduleInfo);
                        keepLoading = true;
                        break;
                    }
                }
            }
        }

        private bool AreDependenciesLoaded(ModuleInfo moduleInfo)
        {
            var requiredModules = ModuleCatalog.GetDependentModules(moduleInfo);
            if (requiredModules == null)
            {
                return true;
            }

            var notReadyRequiredModuleCount =
                requiredModules.Count(requiredModule => requiredModule.State != ModuleState.Initialized);

            return notReadyRequiredModuleCount == 0;
        }

        private void InitializeModule(ModuleInfo moduleInfo)
        {
            if (moduleInfo.State != ModuleState.Initializing) return;
            _moduleInitializer.Initialize(moduleInfo);
            moduleInfo.State = ModuleState.Initialized;
            RaiseLoadModuleCompleted(moduleInfo, null);
        }

        private void BeginRetrievingModule(ModuleInfo moduleInfo)
        {
            var moduleInfoToLoadType = moduleInfo;
            var moduleTypeLoader = GetTypeLoaderForModule(moduleInfoToLoadType);
            moduleInfoToLoadType.State = ModuleState.LoadingTypes;

            // We only want to subscribe to each instance once.
            if (!_subscribedToModuleTypeLoaders.Contains(moduleTypeLoader))
            {
                moduleTypeLoader.LoadModuleCompleted += ModuleTypeLoader_LoadModuleCompleted;
                _subscribedToModuleTypeLoaders.Add(moduleTypeLoader);
            }

            moduleTypeLoader.LoadModuleType(moduleInfo);
        }

        private void ModuleTypeLoader_LoadModuleCompleted(object sender, LoadModuleCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if ((e.ModuleInfo.State != ModuleState.Initializing) && (e.ModuleInfo.State != ModuleState.Initialized))
                {
                    e.ModuleInfo.State = ModuleState.ReadyForInitialization;
                }

                // This callback may call back on the UI thread, but we are not guaranteeing it.
                // If you were to add a custom retriever that retrieved in the background, you
                // would need to consider dispatching to the UI thread.
                LoadModulesThatAreReadyForLoad();
            }
            else
            {
                RaiseLoadModuleCompleted(e);

                // If the error is not handled then I log it and raise an exception.
                if (!e.IsErrorHandled)
                {
                    HandleModuleTypeLoadingError(e.ModuleInfo, e.Error);
                }
            }
        }

        private IModuleTypeLoader GetTypeLoaderForModule(ModuleInfo moduleInfo)
        {
            var typeLoader = ModuleTypeLoaders.FirstOrDefault(loader => loader.CanLoadModuleType(moduleInfo));
            if (typeLoader != null)
                return typeLoader;

            throw new ModuleTypeLoaderNotFoundException(moduleInfo.ModuleName,
                String.Format(CultureInfo.CurrentCulture, Resources.NoRetrieverCanRetrieveModule, moduleInfo.ModuleName),
                null);
        }

        /// <summary>
        ///     Checks if the module needs to be retrieved before it's initialized.
        /// </summary>
        /// <param name="moduleInfo">Module that is being checked if needs retrieval.</param>
        /// <returns></returns>
        protected virtual bool ModuleNeedsRetrieval(ModuleInfo moduleInfo)
        {
            moduleInfo.EnsureParameterNotNull("moduleInfo");

            if (moduleInfo.State != ModuleState.NotStarted) return false;
            // If we can instantiate the type, that means the module's assembly is already loaded into
            // the AppDomain and we don't need to retrieve it.
            var isAvailable = Type.GetType(moduleInfo.ModuleType) != null;
            if (isAvailable)
            {
                moduleInfo.State = ModuleState.ReadyForInitialization;
            }

            return !isAvailable;
        }

        /// <summary>
        ///     Disposes the associated <see cref="IModuleTypeLoader" />s.
        /// </summary>
        /// <param name="disposing">When <see langword="true" />, it is being called from the Dispose method.</param>
        protected virtual void Dispose(bool disposing)
        {
            foreach (var disposableTypeLoader in ModuleTypeLoaders.OfType<IDisposable>())
            {
                disposableTypeLoader.Dispose();
            }
        }

        private void RaiseLoadModuleCompleted(ModuleInfo moduleInfo, Exception error)
        {
            RaiseLoadModuleCompleted(new LoadModuleCompletedEventArgs(moduleInfo, error));
        }

        private void RaiseLoadModuleCompleted(LoadModuleCompletedEventArgs e)
        {
            if (LoadModuleCompleted != null)
            {
                LoadModuleCompleted(this, e);
            }
        }

        /// <summary>
        ///     Handles any exception ocurred in the module typeloading process,
        ///     logs the error using the <seealso cref="ILoggerFacade" /> and throws a
        ///     <seealso cref="ModuleTypeLoadingException" />.
        ///     This method can be overriden to provide a different behavior.
        /// </summary>
        /// <param name="moduleInfo">The module metadata where the error happenened.</param>
        /// <param name="exception">The exception thrown that is the cause of the current error.</param>
        /// <exception cref="ModuleTypeLoadingException"></exception>
        protected virtual void HandleModuleTypeLoadingError(ModuleInfo moduleInfo, Exception exception)
        {
            if (moduleInfo == null) throw new ArgumentNullException("moduleInfo");

            var moduleTypeLoadingException = exception as ModuleTypeLoadingException ??
                                             new ModuleTypeLoadingException(moduleInfo.ModuleName, exception.Message,
                                                 exception);

            _loggerFacade.Log(moduleTypeLoadingException.Message, Category.Exception, Priority.High);

            throw moduleTypeLoadingException;
        }
    }
}