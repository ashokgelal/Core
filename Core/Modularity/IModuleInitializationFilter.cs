﻿namespace Prismactic.Core.Modularity
{
    /// <summary>
    /// An interface to determine whether a <see cref="ModuleInfo"/> can be initialized or not.
    /// </summary>
    public interface IModuleInitializationFilter
    {
        /// <summary>
        ///  Determines whether a <paramref name="moduleInfo"/> can be initialized or not.
        /// </summary>
        /// <param name="moduleInfo">Module that should have it's type loaded.</param>
        /// <returns><see langword="true"/> if the module can be initialized, otherwise <see langword="false"/>.</returns>
        bool ShouldInitializeModule(ModuleInfo moduleInfo);
    }
}