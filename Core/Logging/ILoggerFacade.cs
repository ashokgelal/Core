using System.Runtime.CompilerServices;

namespace Prismactic.Core.Logging
{
    /// <summary>
    ///     Defines a simple logger fa�ade to be used by the Composite Application Library.
    /// </summary>
    public interface ILoggerFacade
    {
        /// <summary>
        ///     Write a new log entry with the specified category and priority.
        /// </summary>
        /// <param name="message">Message body to log.</param>
        /// <param name="category">Category of the entry.</param>
        /// <param name="priority">The priority of the entry.</param>
        /// <param name="memberName">The member logging this log. The caller should almost never pass this value.</param>
        /// <param name="sourceFilePath">The source file path logging this log. The caller should almost never pass this value.</param>
        /// <param name="sourceLineNumber">The source line number from where this log is being logged. The caller should almost never pass this value.</param>
        void Log(string message, Category category, Priority priority,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string sourceFilePath = "",
            [CallerLineNumber] int sourceLineNumber = 0);
    }
}