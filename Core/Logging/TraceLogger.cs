using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace Prismactic.Core.Logging
{
    /// <summary>
    ///     Implementation of <see cref="ILoggerFacade" /> that logs to .NET <see cref="Trace" /> class.
    /// </summary>
    public class TraceLogger : ILoggerFacade
    {
        /// <summary>
        ///     Write a new log entry with the specified category and priority.
        /// </summary>
        /// <param name="message">Message body to log.</param>
        /// <param name="category">Category of the entry.</param>
        /// <param name="priority">The priority of the entry.</param>
        /// <param name="memberName">The member logging this log.</param>
        /// <param name="sourceFilePath">The source file path logging this log.</param>
        /// <param name="sourceLineNumber">The source line number from where this log is being logged.</param>
        public void Log(string message, Category category, Priority priority,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string sourceFilePath = "",
            [CallerLineNumber] int sourceLineNumber = 0)
        {
            if (category == Category.Exception)
            {
                Trace.TraceError(message);
            }
            else
            {
                Trace.TraceInformation(message);
            }
        }
    }
}