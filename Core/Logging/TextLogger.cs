using System;
using System.Globalization;
using System.IO;
using System.Runtime.CompilerServices;
using Prismactic.Core.Extensions;
using Prismactic.Core.Properties;

namespace Prismactic.Core.Logging
{
    /// <summary>
    ///     Implementation of <see cref="ILoggerFacade" /> that logs into a <see cref="TextWriter" />.
    /// </summary>
    public class TextLogger : ILoggerFacade, IDisposable
    {
        private readonly TextWriter _writer;

        /// <summary>
        ///     Initializes a new instance of <see cref="TextLogger" /> that writes to
        ///     the console output.
        /// </summary>
        public TextLogger()
            : this(Console.Out)
        {
        }

        /// <summary>
        ///     Initializes a new instance of <see cref="TextLogger" />.
        /// </summary>
        /// <param name="writer">The writer to use for writing log entries.</param>
        public TextLogger(TextWriter writer)
        {
            writer.EnsureParameterNotNull("writer");
            _writer = writer;
        }

        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <remarks>Calls <see cref="Dispose(bool)" /></remarks>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///     Write a new log entry with the specified category and priority.
        /// </summary>
        /// <param name="message">Message body to log.</param>
        /// <param name="category">Category of the entry.</param>
        /// <param name="priority">The priority of the entry.</param>
        /// <param name="memberName">The member logging this log.</param>
        /// <param name="sourceFilePath">The source file path logging this log.</param>
        /// <param name="sourceLineNumber">The source line number from where this log is being logged.</param>
        public void Log(string message, Category category, Priority priority,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string sourceFilePath = "",
            [CallerLineNumber] int sourceLineNumber = 0)
        {
            var messageToLog = String.Format(CultureInfo.InvariantCulture, Resources.DefaultTextLoggerPattern,
                category.ToString().ToUpper(CultureInfo.InvariantCulture),
                message, priority, sourceLineNumber, memberName, sourceFilePath, DateTime.Now);

            _writer.WriteLine(messageToLog);
        }

        /// <summary>
        ///     Disposes the associated <see cref="TextWriter" />.
        /// </summary>
        /// <param name="disposing">When <see langword="true" />, disposes the associated <see cref="TextWriter" />.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposing) return;
            if (_writer != null)
            {
                _writer.Dispose();
            }
        }
    }
}