﻿#region usings

using System;
using Microsoft.Practices.ServiceLocation;
using Prismactic.Core.Extensions;
using Prismactic.Core.Logging;
using Prismactic.Core.Modularity;
using Prismactic.Core.Properties;

#endregion

namespace Prismactic.Core.Bootstrapper
{
    public abstract class PrismacticCoreBootstrapper
    {
        /// <summary>
        ///     Gets the <see cref="ILoggerFacade" /> for the application.
        /// </summary>
        /// <value>A <see cref="ILoggerFacade" /> instance.</value>
        protected ILoggerFacade Logger { get; set; }

        /// <summary>
        ///     Gets the default <see cref="IModuleCatalog" /> for the application.
        /// </summary>
        /// <value>The default <see cref="IModuleCatalog" /> instance.</value>
        protected IModuleCatalog ModuleCatalog { get; set; }

        /// <summary>
        ///     Creates the <see cref="IModuleCatalog" /> used by Prism.
        /// </summary>
        /// <remarks>
        ///     The base implementation returns a new ModuleCatalog.
        /// </remarks>
        protected virtual IModuleCatalog CreateModuleCatalog()
        {
            return new ModuleCatalog();
        }

        /// <summary>
        ///     Configures the <see cref="IModuleCatalog" /> used by Prism.
        /// </summary>
        protected virtual void ConfigureModuleCatalog()
        {
        }

        /// <summary>
        ///     Initializes the modules. May be overwritten in a derived class to use a custom Modules Catalog
        /// </summary>
        protected virtual void InitializeModules()
        {
            var manager = ServiceLocator.Current.GetInstance<IModuleManager>();
            manager.Run();
        }

        /// <summary>
        ///     Create the <see cref="ILoggerFacade" /> used by the bootstrapper.
        /// </summary>
        /// <remarks>
        ///     The base implementation returns a new TextLogger.
        /// </remarks>
        protected virtual ILoggerFacade CreateLogger()
        {
            var logger = new TextLogger();
            logger.Log(Resources.DefaultTextLoggerCreatedSuccessfully, Category.Debug, Priority.Low);
            return logger;
        }

        /// <summary>
        ///     Registers the <see cref="Type" />s of the Exceptions that are not considered
        ///     root exceptions by the <see cref="ExceptionExtensions" />.
        /// </summary>
        protected virtual void RegisterFrameworkExceptionTypes()
        {
            ExceptionExtensions.RegisterFrameworkExceptionType(
                typeof (ActivationException));
        }

        /// <summary>
        ///     Run the bootstrapper process.
        /// </summary>
        /// <param name="runWithDefaultConfiguration">
        ///     If <see langword="true" />, registers default
        ///     Composite Application Library services in the container. This is the default behavior.
        /// </param>
        public abstract void Run(bool runWithDefaultConfiguration);

        /// <summary>
        ///     Configures the LocatorProvider for the <see cref="Microsoft.Practices.ServiceLocation.ServiceLocator" />.
        /// </summary>
        /// <remarks>
        ///     The base implementation also sets the ServiceLocator provider singleton.
        /// </remarks>
        protected abstract void ConfigureServiceLocator();

        /// <summary>
        /// Initializes the shell.
        /// </summary>
        protected virtual void InitializeShell()
        {
        }
    }
}